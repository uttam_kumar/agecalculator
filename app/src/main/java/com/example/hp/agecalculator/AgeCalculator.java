package com.example.hp.agecalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class AgeCalculator extends AppCompatActivity {


    private Button calculateButton, refreshButton;

    private String bday_selector,bmonth_selector,byear_selector,tday_selector,tmonth_selector,tyear_selector;

    private int bday=0,bmonth=0,byear=0,tday=0,tmonth=0,tyear=0,day=0,month=0,year=0;

    private Spinner bDaySpinner,bMonthSpinner,bYearSpinner,tDaySpinner,tMonthSpinner,tYearSpinner;

    private String[] day_number;
    private String[] month_number;
    private String[] year_number;


    private TextView dayTextView,monthTextView,yearTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_age_calculator);


        dayTextView=findViewById(R.id.dayTextViewId);
        monthTextView=findViewById(R.id.monthTextViewId);
        yearTextView=findViewById(R.id.yearTextViewId);

        calculateButton = findViewById(R.id.calculateButtonId);
        refreshButton = findViewById(R.id.refreshButtonId);

        day_number= getResources().getStringArray(R.array.day_number);
        month_number= getResources().getStringArray(R.array.month_number);
        year_number= getResources().getStringArray(R.array.year_number);


        bDaySpinner=findViewById(R.id.bdaySpinnerId);
        bMonthSpinner=findViewById(R.id.bmonthSpinnerId);
        bYearSpinner=findViewById(R.id.byearSpinnerId);

        tDaySpinner=findViewById(R.id.tdaySpinnerId);
        tMonthSpinner=findViewById(R.id.tmonthSpinnerId);
        tYearSpinner=findViewById(R.id.tyearSpinnerId);


        final ArrayAdapter<String> dayAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,day_number);
        bDaySpinner.setAdapter(dayAdapter);
        tDaySpinner.setAdapter(dayAdapter);

        final ArrayAdapter<String> monthAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,month_number);
        bMonthSpinner.setAdapter(monthAdapter);
        tMonthSpinner.setAdapter(monthAdapter);

        final ArrayAdapter<String> yearAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,year_number);
        bYearSpinner.setAdapter(yearAdapter);
        tYearSpinner.setAdapter(yearAdapter);

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bDaySpinner.setAdapter(dayAdapter);
                tDaySpinner.setAdapter(dayAdapter);
                bMonthSpinner.setAdapter(monthAdapter);
                tMonthSpinner.setAdapter(monthAdapter);
                bYearSpinner.setAdapter(yearAdapter);
                tYearSpinner.setAdapter(yearAdapter);
            }
        });

        bDaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bday_selector=day_number[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bMonthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bmonth_selector=month_number[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        bYearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                byear_selector=year_number[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        tDaySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tday_selector=day_number[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        tMonthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tmonth_selector=month_number[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        tYearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                tyear_selector=year_number[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        calculateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            try{


                if(bday_selector!="dd"&&tday_selector!="dd"&&bmonth_selector!="mm"
                        &&tmonth_selector!="mm"&&byear_selector!="yy"&&tyear_selector!="yy"){
                    bday=Integer.parseInt(bday_selector);
                    tday=Integer.parseInt(tday_selector);

                    bmonth=Integer.parseInt(bmonth_selector);
                    tmonth=Integer.parseInt(tmonth_selector);

                    byear=Integer.parseInt(byear_selector);
                    tyear=Integer.parseInt(tyear_selector);
                    if(byear>tyear){
                        Toast.makeText(AgeCalculator.this, "Enter valid date", Toast.LENGTH_SHORT).show();
                    }else{
                        if(tday<bday){
                            tday=tday+30;
                            tmonth=tmonth-1;
                        }
                        if(tmonth<bmonth){
                            tmonth=tmonth+12;
                            tyear=tyear-1;
                        }
                    }

                    day=tday-bday;
                    month=tmonth-bmonth;
                    year=tyear-byear;

                    Log.d("AgeCalculator", "onClick: bday="+bday+"\ntday="+tday+"\nbmonth="+bmonth+"\ntmonth="+tmonth);

                }else{
                    Toast.makeText(AgeCalculator.this, "Enter valid date", Toast.LENGTH_SHORT).show();
                }
                dayTextView.setText(day+"");
                monthTextView.setText(month+"");
                yearTextView.setText(year+"");
            }catch (Exception e){
                Toast.makeText(AgeCalculator.this, "Enter valid date", Toast.LENGTH_SHORT).show();
            }

            }


        });
    }

}
